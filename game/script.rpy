﻿# Characters
define mc = Character("[mcname] [mcfname]")
define adam = Character("Adam Kapowski",image="adam")
define annie = Character("Annie Sagan",image="annie")
define beowulf = Character("Mr. Grendel",image="beowulf")
define bigband = Character("Mr. Birdland",image="bigband")
define cerebella = Character("Cerebella Vice-Versa",image="cerebella")
define double = Character("Ms. Double",image="double")
define eliza = Character("Ms. Sekhmet",image="eliza")
define filia = Character("Filia Medici",image="filia")
define fortune = Character("Nadia Fortune",image="fortune")
define fukua = Character("Fukua Medici",image="fukua")
define marie = Character("Marie Heart",image="marie")
define minette = Character("Minette Gills",image="minette")
define painwheel = Character("Carol Painwheel",image="painwheel")
define parasoul = Character("Principal Renoir",image="parasoul")
define peacock = Character("Patricia 'Peacock' Watson",image="peacock")
define robo = Character("Coffee-Fortune",image="robo")
define squigly = Character("Sienna 'Squigly' Contiello",image="squigly")
define umbrella = Character("Umbrella Renoir",image="umbrella")
define valentine = Character("Ms. Valentine",image="valentine")

label start:
    # Name definition
    $mcname = renpy.input("What is your first name?")
    $mcname = mcname.strip()
    if not mcname:
        $mcname = "Romain"

    $mcfname = renpy.input("What is your family name?")
    $mcfname = mcfname.strip()
    if not mcfname:
        $mcfname = "Fecher"

    # First scene in mc-kun's room talking to himself, background exposition
    scene schoolnursebgtemp # it's supposed to be a room

    "Today is my first day in my new school, Canopy College. {w} I made sure to get up early today because I don’t want to be late."
    "I won’t have to get up as early the next days because I will be residing in dorms on the campus. {w} I can’t stay at my parents' this year because we live too far away from my new school."
    "Two years ago, just after I graduated from my previous school, we had to move to New Meridian for my father’s job."
    "Now that we are back in Canopolis I had to enter a new college and that is when I got accepted in Canopy College. {w} The students I will be with this year will be in their third year but the principal said that with my level I wouldn’t have any problems with the transition."
    "I just hope she’s right..."
    "I should hurry, I’m wasting time here. {w} If I don’t stop, waking up this early will have been for nothing."
    "I just have to check one last time my suitcase..."
    "After saying goodbye to my parents I got on to the first train going to the school grounds."

    # Scene 2 in front of school mc looking for friends and sees Filia
    scene schoolfrontbgtemp

    "I just dropped off my bags in my new room, {w} I now need to head to the principal’s office."
    "As i arrive at the front gates I start to look around me at all the students to see if I would know anyone. {w} It’s highly unlikely but seeing how many students there are I still think there may be a chance that I recognize someone from my old school or something."
    "..."
    "As I suspected... {w} no one..."
    "I make my way to the entrance of the school. {w} It’s at that moment that in the corner of my eye, I notice a very familiar figure..."
    "Filia!"

    menu:
        "I don't want to be late if we talk for too long. Should I go talk to her?"
        "At least say hi to her.": # +affection with filia and parasoul
            # Scene 3 filia route

            "Ok, I think I should go and say hi at least. {w} It won't take too long."

            show filia smile at right with dissolve

            mc "Hi… {w} Filia."

            filia happy "Hello…"

            filia surprised "Oh [mcname]! {w} Hi! {w} You’re attending this school now? {w} Cool! It’s been too long since the last time..."

            mc "Yeah, something like three years."

            "We’ve been friends for quite some time so I was really sad when we lost contact three years ago when she left to study abroad for one year.{w} So I’m really happy to see that she is attending this school to."

            filia happy "Hm… three years. {w} How’ve you been?"

            mc "Apart from difficulty of having to adjust quickly and adapt to my new environment each time I move I've been all right. And how about you?"

            filia smile "I'm doing great! I’ve made a lot of new friends in this school. {w} I’ll be able to present them to you if you want, and I even think we might be in the same class. Wouldn’t that be great?"

            filia concerned "Crap it’s almost time to go to classes."

            mc "Oh yes! I didn’t see the time… {w} That being said, I first need to go to the principal’s office. {w} I don’t really know where it is, could you tell me the quickest way to get there?"

            filia happy "Do you want me to take you there?"

            mc "You don’t have to do that, you’ll be late for classes because of me."

            filia "Don’t worry, it’s not that far. And anyway, if I’m late Nadia can tell the teacher I was helping you out!"

            hide filia with dissolve

            show fortune at right with dissolve

            fortune "Yeah, no problem!"

            hide fortune with dissolve

            show filia happy at right with dissolve

            filia "Don’t worry, I’m happy I can help you."

            mc "Thanks."

            # Scene 4 Filia route (school corridors)

            "After a very brisk walk we arrive in front of a door with the readings ‘Principal’s Office’ on it."

            filia "We're here! {w} Told you it wasn’t far."

            mc "Thanks a lot. Your not even going to be late to class!"

            filia "You see I told you not to worry."
            filia "If you want, for lunch we can eat together with Nadia."

            mc "Yeah, that would be really nice."

            "Since I don’t know anyone else yet and I don’t know where to get food it would save me from not eating lunch today."

            filia "Cool! {w} Anyway see you later!"

            mc "See you later."

            hide filia with dissolve

            "I’m a bit early but I don’t think she will mind. {w} Ok, here I go."
            "*Knock Knock*"

            parasoul "Come in."

            # Scene 5 Parasoul route (Principal's office)

            show parasoul happy at right with dissolve

            mc "Hello. Excuse me, I’m [mcname] [mcfname]. {w} Sorry, I’m a bit early."

            parasoul "Oh hello [mcname], don’t worry. {w} It’s ok, better early than late. {w} You didn’t have too much trouble finding my office?"

            mc "No I met an old friend and she helped me find your office."

            parasoul ""

            jump scene5

        "Go strait to the principal's office.": # -affection with parasoul
            $principal_office = True

            "I shouldn’t waste time. In any case we will be here all year so it’s not like I won’t have time to see her again."

            # Scene 3 main route (school corridors)

            "I've been walking around these corridors for at least 20 minutes..."
            "Man this is not good, I can’t find the principal’s office. {w} I should of asked someone but they are all in class now…"
            "This is bad, already late on my first day..."
            "Oh great, A map… {w} I should be able to find the way with this."
            "Found it!"
            "I knock on the door. {w} *Knock Knock*"

            parasoul "Come in!"

            # Scene 4 main route (principal's office)

            "I hope she won’t be too mad."

            mc "Hello. Excuse me, I’m very sorry for being late. I had trouble finding your office."

            parasoul "Hello. You must be the new student, [mcname] [mcfname]."

            mc "Yes, that’s me."

            parasoul "My name is Parasoul Renoir and I will be your Principal for this year. {w} You know, there are maps of the layout on every floor?"

            mc "Yes, I noticed, but sadly a bit late."

            parasoul "Anyway, now that you are here we can start. {w} Here are some papers you need to fill out as soon as possible and bring them back to me when you are finished."

            mc "Right. {w} Thank you."

            parasoul "Another thing, if ever you having trouble or have questions about somethings don’t hesitate to ask me or your teachers."

            mc "I will, thank you."

            parasoul "I will now take you to your class. Your main teacher will Ms. Sekhmet, and you are in class 2B. {w} Now will you please follow me, you musn't be even more late to your first class than you are already."

            mc "I’m sorry about that."

            "Good job, great first impressions."


    label scene5:

        # Scene 5; start black screen

        "Principal Renoir knocks on the door to the classroom. {w} *Knock Knock*"

        eliza "Yes?"

        # scene classroom

        parasoul "Good Morning Ms. Sekhmet sorry for interrupting your class but I’ve brought you your new student."

        eliza "Oh splendid, thank you Principal Renoir! {w} Please come in don’t be shy… {w} I don’t bite."

        parasoul "Well, I’ll leave you here. Have a good first day, remember if you have any questions make sure to ask your teacher and if you have any trouble you can always come and talk to me."
        parasoul "Sorry again for the interruption, I’ll be on my way."

        eliza "No problem, and don’t worry we’ll take good care of him, won’t we class?"

        "Principal Renoir leaves the room."

        eliza "Now would you like to present yourself to the class?"

        "As I get ready to present myself I take a quick glance around the class to see if there are any familiar faces… {w} Thankfully I notice that Filia is in my class."

        if principal_office:
            "She’s a friend from childhood we lost contact three years ago when she left to study abroad for one year."

        mc "Ok… {w} Hello my name is [mcname] [mcfname], I moved to New Meridian two years ago because of my father’s work but now we are back."

    return # This ends the game.
